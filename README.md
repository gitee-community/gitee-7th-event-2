  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/174156_8c1a9cdd_1899542.png "悬赏令.png")  
**[活动主会场 >>](https://oschina.gitee.io/gitee-7th/?utm_source=gitee-event-2):sparkles:** 

  
#### 活动介绍
通过解决开源软件 issue（修改bug 、新增功能等）参与开源贡献，解决不同难度的 issue 可获得相应礼品。  
  
#### 主办方  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/180153_692e0ecf_1899542.png "logo.png")
  
#### 活动流程
1、题库征集：开源软件作者/软件维护者选取软件中需要协助的 issue 提交至活动题库（如 bug 、功能等）  
时间：5月27日 - 6月1日       
  
2、题库生成：Gitee 统一审核 issue ，收录符合条件的 issue 至活动题库（ 题库将按 issue 难度划分（困难、中等、简单），题库集满为止 ）   
  
3、题库解决：参赛者通过完成待解决的 issue 参与开源贡献，解决不同难度的 issue 可获得与难度相应的的奖品  
时间：6月1日 - 6月22日  
  
4、题库审核: 开源软件作者/软件维护者统一审核活动提交的 PR   
时间：6月23日 - 6月30日

5、公布获奖名单，安排奖品邮寄事宜    
获奖公布时间：7月3日
  
#### 活动题库：待解决的 issue 

|  issue 难度   |   征集个数  |  参赛 issue   | issue 所属软件 |  完成奖励 |
| --- | --- | --- |  ---| ---|
|  困难   |  3   |   [点击查看](https://gitee.com/gitee-community/gitee-7th-event-2/blob/master/issue-difficult.md)  | Apache DolphinScheduler、RT-Thread、SOFAJRaft|  Apple ipad / SONY 数码相机/ 2000 元京东购物卡  |
|  中等   |  9   |  [点击查看](https://gitee.com/gitee-community/gitee-7th-event-2/blob/master/issue-medium.md)   | Bootstrap Blazor、 jeecg-boot 、OpenAuth.Core、Jenkins CLI、Dataway、Apache DolphinScheduler、SOFABolt、RT-Thread、kkfileview | 罗技（G）机械键盘/SKG 颈椎按摩器/500 元京东卡  |
|  简单   |  3   |  [点击查看](https://gitee.com/gitee-community/gitee-7th-event-2/blob/master/issue-simple.md)   | RT-Thread、Swiper3d | Gitee T恤 / 100元京东购物卡 |

### 参与方式
**开源作者/软件维护者：**   
1. 提交 issue  
开源作者可以选取软件中需要他人协助的 issue 提交进题库
2. 审核 issue PR: 6 月 23 日，对参与本次活动的 issue 提交的 Pull Request 进行审核。

**参赛者：**  :point_right: [帮助文档](https://gitee.com/help/categories/42)  
1. 在待解决的 issue 中选择一个或多个，解决它。
2. 解决 issue 后，再 issue 所属仓库提交 Pull Request 并复制 issue ID 至 PR 描述，点击“提交审核”按钮，注意：请备注【开源贡献活动】关键字（注：在 issue 所属仓库内提交即可，无需在本仓库提交）
3. 开源软件作者会在 6 月 23 日 审核提交的 Pull Request，Pull Request审核通过的小伙伴即可获得该 issue 对应的奖品

#### 注意
开源软件作者/软件维护者解决参赛 issue 不计入获奖范围

####  :gift: 活动奖励
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/104140_e9912ec0_1899542.png "修改奖品图片-奖品设置.png")



-----------------------------
### 获奖名单公布 
| 复杂度  | issue | 所属开源软件  | 获奖用户  |
|---|---|---|---|
| 困难  | 增加imx6ull BSP：https://gitee.com/rtthread/rt-thread/issues/I1I3VP  | RT-Thread  | [David-dwd](https://gitee.com/DAI_David)   |
| 困难 |  https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I1I5C9 | Apache DolphinScheduler  | [Rubik-W](https://gitee.com/rubik-w)  |
| 中等  | https://gitee.com/LongbowEnterprise/BootstrapBlazor/issues/I1H3PO?from=project-issue  | Bootstrap Blazor  | [一事冇诚](https://gitee.com/Ysmc)  |
| 中等  | https://gitee.com/rtthread/rt-thread/issues/I1I3UR  | RT-Thread  | [来日方长](https://gitee.com/NU-LL) | 
| 中等  | https://gitee.com/yubaolee/OpenAuth.Core/issues/I1DOWV  | OpenAuth.Core  | [CodeMan-P](https://gitee.com/CodeMan-P)  | 
| 中等  | https://gitee.com/kekingcn/file-online-preview/issues/I1I5SZ  | kkfileview  | [点蚊子熏烟](https://gitee.com/dpchenheng)  |
| 中等  | https://gitee.com/jeecg/jeecg-boot/issues/I1ARZ0  |  jeecg-boot | [Jixq](https://gitee.com/jixq)  |
| 简单  | https://gitee.com/visitor009/swiper3d/issues/I18M17  |  Swiper3d | [tom](https://gitee.com/ChineseTom)  |
  
恭喜以上用户获奖！已通过私信联系大家，请及时回复：）  
以上是已通过开源软件作者的审核并完成合并的 issue，有任何疑问请联系开源软件作者与主办方：）
 