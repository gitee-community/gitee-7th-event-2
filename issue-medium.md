#### 中等级 issue :fa-star: :fa-star:  :fa-star-o: 
解决以下任意一个 issue 即可获得  :sparkles:  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/163305_289b70f4_1899542.png "屏幕截图.png")  
 
#### 活动详情
[点击前往 >>](https://gitee.com/gitee-community/gitee-7th-event-2)
  
#### 参与方式
1. 点击 issue 链接查看详情
2. 解决 issue 后，在 issue 所属仓库提交 Pull Request 并复制 issue ID 至 PR 描述，点击“提交审核”，注意：请备注【开源贡献活动】关键字（注：在 issue 所属仓库内提交即可，无需在本活动仓库再次提交）
3. 6月23日，开源软件作者/团队统一审核提交的 Pull Request，Pull Request 审核通过的小伙伴即可获得该 issue 对应的奖品

| 复杂度  | 类型  |  issue 描述 | issue 链接  | 所属软件 |
|---|---|---|---|---|
|  中等|新功能 | 增加一个 Markdown 组件  |  https://gitee.com/LongbowEnterprise/BootstrapBlazor/issues/I1H3PO?from=project-issue |  Bootstrap Blazor |
|  中等 | bug  |上传接口在swagger ui中实现上传的问题  | https://gitee.com/yubaolee/OpenAuth.Core/issues/I1DOWV  |  OpenAuth.Core |
| 中等 | 新功能  | 通过命令行可以实现触发 Jenkins 带有文件参数的流水线  | https://gitee.com/jenkins-zh/jenkins-cli/issues/I1I4KE  |  Jenkins CLI |
| 中等 | 新功能  |  增加对ES支持 | https://gitee.com/zycgit/hasor/issues/I1FHM5  |  Dataway |
| 中等 | 新功能  | "目前 DS 仅能通过命令行方式部署，对用户要求较高。提供可视化部署页面有助于降低用户使用门槛。可视化部署主要包括：1. 部署环境检测及初始化2. DS 配置初始化3. 通用页面启停 master/worker/logger/alert 等 DS 相关组件"  | https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I1I5N9  | Apache DolphinScheduler(Incubating)  |
| 中等  | 改进  | "当前的SOFABolt实现中私有协议是直接和整体框架耦合在一起的，在使用SOFABolt的同时需要使用提供的私有协议，当用户希望使用自己的自定义协议时需要进行比较有挑战的代码拓展才能实现。因此我们希望对SOFABolt的协议框架做一些重构以支持快速方便的集成用户的自定义协议。- 解耦协议框架和具体私有协议实现"  | https://gitee.com/sofastack/sofa-bolt/issues/I1I97K  | SOFABolt  |
| 中等  |  新功能 |  增加线程栈静态分析功能 | https://gitee.com/rtthread/rt-thread/issues/I1I3UR  |  RT-Thread  |
| 中等  |  bug |  这是一个文件预览项目，在预览pdf时，如果pdf携带签章信息，会显示不全面，可能是pdf.js的问题，等你来修复 | https://gitee.com/kekingcn/file-online-preview/issues/I1I5SZ  | kkfileview  |
| 中等 |   bug |  使用多页签模式，打开系统管理中的 用户管理 和 角色管理 页面，使用tab页面切换来回在用户管理和角色管理页面切换几次之后，在左侧菜单中点击首页，此时首页内容出现渲染问题 | https://gitee.com/jeecg/jeecg-boot/issues/I1ARZ0  | jeecg-boot  |
  
[查看更多悬赏中的 issue >>](https://gitee.com/gitee-community/gitee-7th-event-2)  