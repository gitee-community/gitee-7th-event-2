#### 简单级 issue :fa-star:  :fa-star-o:  :fa-star-o: 
解决以下任意一个 issue 即可获得  :sparkles:  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/164539_8d4f8c23_1899542.png "屏幕截图.png")
#### 活动详情
[点击前往 >>](https://gitee.com/gitee-community/gitee-7th-event-2)
#### 参与方式
1. 点击 issue 链接查看详情
2. 解决 issue 后，在 issue 所属仓库提交 Pull Request 并复制 issue ID 至 PR 描述，点击“提交审核”，注意：请备注【开源贡献活动】关键字（注：在 issue 所属仓库内提交即可，无需在本活动仓库再次提交）
3. 6月23日，开源软件作者/团队统一审核提交的 Pull Request，Pull Request 审核通过的小伙伴即可获得该 issue 对应的奖品

| 复杂度  | 类型  |  issue 描述 | issue 链接  | 所属软件 |
|---|---|---|---|---|
|简单|bug | can驱动有问题 一直卡在rt_completion_wait() | https://gitee.com/rtthread/rt-thread/issues/I1FAWH | RT-Thread | 
|简单|bug|RISC-V编译器编译K210 提示头文件冲突|https://gitee.com/rtthread/rt-thread/issues/I1BFQG|RT-Thread|
|简单|bug|CSS3 移动端兼容性问题，在ios13.1.3夸克浏览器下层级过高，设置z-index无效|（注：此 issue 已解决）https://gitee.com/visitor009/swiper3d/issues/I18M17|Swiper3d|  
[查看更多悬赏中的 issue >>](https://gitee.com/gitee-community/gitee-7th-event-2)  